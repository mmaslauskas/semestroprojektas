package com.ktu.semestroprojektas.repositories;

import com.ktu.semestroprojektas.models.Order;
import com.ktu.semestroprojektas.models.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {

    @Query(value = "SELECT * FROM ORDERS O WHERE O.DISPATCHER_ID=:id",
    countQuery = "SELECT COUNT(*) FROM ORDERS WHERE DISPATCHER_ID=:id",
    nativeQuery = true)
    Page<Order> getDispatcherOrders(@Param("id") Long dispatcherId, Pageable pageable);

    @Query(value = "SELECT * FROM ORDERS O " +
            "WHERE (O.STATUS='Available' OR (O.STATUS='Accepted' AND O.COURIER_ID=:id) OR " +
            "(O.STATUS='In transit' AND O.COURIER_ID=:id)) AND (O.ID) NOT IN" +
                                                                            " (SELECT RO.ORDER_ID " +
                                                                                "FROM REJECTEDORDERS RO " +
                                                                                    " WHERE RO.COURIER_ID=:id) " +
            "ORDER BY O.STATUS DESC",
            countQuery = "SELECT COUNT(*) FROM ORDERS O " +
                    "WHERE (O.STATUS='Available' OR (O.STATUS='Accepted' AND O.COURIER_ID=:id) OR " +
                    "(O.STATUS='In transit' AND O.COURIER_ID=:id)) AND (O.ID) NOT IN" +
                                                                                "(SELECT RO.ORDER_ID" +
                                                                                    "FROM REJECTEDORDERS RO" +
                                                                                        "WHERE RO.COURIER_ID=:id)",
            nativeQuery = true)
    Page<Order> getCourierOrders(@Param("id") Long courierId, Pageable pageable);

    @Transactional
    @Modifying
    @Query("Update Order o SET o.courier=:courier, o.dispatcher=:dispatcher, o.status=:status WHERE o.id=:id")
    void updateOrder(@Param("id") Long id, @Param("dispatcher") User dispatcher, @Param("courier") User courier, @Param("status") String status);

    @Query(value = "SELECT DISTINCT TITLE FROM ORDERS",
            countQuery = "SELECT DISTINCT COUNT(TITLE) FROM ORDERS",
            nativeQuery = true)
    List<String> getDistinctTitles();

    @Query(value = "SELECT DISTINCT CATEGORY FROM ORDERS",
            countQuery = "SELECT DISTINCT COUNT(CATEGORY) FROM ORDERS",
            nativeQuery = true)
    List<String> getDistinctCategories();

    @Query(value = "SELECT DISTINCT STATUS FROM ORDERS WHERE STATUS IS NOT NULL",
            countQuery = "SELECT DISTINCT COUNT(STATUS) FROM ORDERS WHERE STATUS IS NOT NULL",
            nativeQuery = true)
    List<String> getDistinctStatus();

    @Query(value = "SELECT * FROM ORDERS O WHERE ((O.STATUS = :status AND :status IS NOT NULL) OR :status IS NULL OR :status='')" +
            "AND ((O.TITLE = :title AND :title IS NOT NULL) OR :title IS NULL OR :title='') " +
            "AND ((O.CATEGORY = :category AND :category IS NOT NULL) OR :category IS NULL OR :category='')" +
            "AND O.PRICE >= CASEWHEN(:priceFrom = '',0,:priceFrom) AND O.PRICE <= CASEWHEN(:priceTo='',500,:priceTo)" +
            "AND (O.DATE_ADDED >= CASEWHEN(:dateFrom='','1900-01-01',:dateFrom) AND O.DATE_ADDED <= CASEWHEN(:dateTo='','9999-12-30',:dateTo)" +
            "AND O.DISPATCHER_ID=:id)",
            nativeQuery = true)
    Page<Order> getFilteredDispatcherOrders(@Param("status") String status, @Param("title") String title,
            @Param("category") String category, @Param("priceFrom") String priceFrom, @Param("priceTo") String priceTo,
            @Param("dateFrom") String dateFrom, @Param("dateTo") String dateTo, @Param("id") Long id, Pageable pageable);

    @Query(value = "SELECT * FROM ORDERS O WHERE ((O.STATUS = :status AND :status IS NOT NULL) OR :status IS NULL OR :status='')" +
            "AND ((O.TITLE = :title AND :title IS NOT NULL) OR :title IS NULL OR :title='') " +
            "AND ((O.CATEGORY = :category AND :category IS NOT NULL) OR :category IS NULL OR :category='')" +
            "AND O.PRICE >= CASEWHEN(:priceFrom = '',0,:priceFrom) AND O.PRICE <= CASEWHEN(:priceTo='',500,:priceTo)" +
            "AND (O.DATE_ADDED >= CASEWHEN(:dateFrom='','1900-01-01',:dateFrom) AND O.DATE_ADDED <= CASEWHEN(:dateTo='','9999-12-30',:dateTo))",
            nativeQuery = true)
    Page<Order> getFilteredOrders(@Param("status") String status, @Param("title") String title,
                                            @Param("category") String category, @Param("priceFrom") String priceFrom, @Param("priceTo") String priceTo,
                                            @Param("dateFrom") String dateFrom, @Param("dateTo") String dateTo, Pageable pageable);

}
