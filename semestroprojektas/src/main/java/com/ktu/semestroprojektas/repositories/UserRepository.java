package com.ktu.semestroprojektas.repositories;

import com.ktu.semestroprojektas.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    User findByEmail(String email);
    List<User> findByRole(String role);

    @Transactional
    @Modifying
    @Query("Update User u SET u.role=:role WHERE u.email=:email")
    void updateRole(@Param("role") String role, @Param("email") String email);
}
