package com.ktu.semestroprojektas.repositories;

import com.ktu.semestroprojektas.models.Comment;
import com.ktu.semestroprojektas.models.RejectedOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RejectedOrderRepository extends JpaRepository<RejectedOrder, Long> {
}
