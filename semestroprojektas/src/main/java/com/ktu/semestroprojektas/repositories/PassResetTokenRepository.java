package com.ktu.semestroprojektas.repositories;

import com.ktu.semestroprojektas.models.PasswordResetToken;
import com.ktu.semestroprojektas.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PassResetTokenRepository extends JpaRepository<PasswordResetToken, Long> {
    PasswordResetToken findByToken(String token);
}
