package com.ktu.semestroprojektas.models;

import javax.persistence.*;

@Entity
@Table(name = "rejectedorders")
public class RejectedOrder
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(targetEntity = Order.class, fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name = "order_id")
    private Order orderR;

    @ManyToOne(targetEntity = User.class, fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name = "courier_id")
    private User courierR;

    public RejectedOrder(){}

    public RejectedOrder(Order order, User user)
    {
        this.courierR = user;
        this.orderR = order;
    }

    public Long getId() {
        return id;
    }

    public Order getOrder() {
        return orderR;
    }

    public void setOrder(Order order) {
        this.orderR = order;
    }

    public User getCourier() {
        return courierR;
    }

    public void setCourier(User courier) {
        this.courierR = courier;
    }
}
