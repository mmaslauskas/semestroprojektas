package com.ktu.semestroprojektas.models;

import javax.persistence.*;
import java.util.Calendar;
import java.util.Date;

@Entity
public class PasswordResetToken {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String token;

    private final int DURATION = 24; //how long(in hours) the reset link will be valid

    @ManyToOne(targetEntity = User.class, fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name = "user_id")
    private User userT;

    private Calendar expiryDate;

    public PasswordResetToken(){}

    public PasswordResetToken(String token, User user)
    {
        this.token = token;
        this.userT = user;
        expiryDate = Calendar.getInstance();
        expiryDate.add(Calendar.HOUR_OF_DAY, DURATION);

    }

    public Long getId() {
        return id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public User getUser() {
        return userT;
    }

    public void setUser(User user) {
        this.userT = user;
    }

    public Calendar getExpiryDate() {
        return expiryDate;
    }


}
