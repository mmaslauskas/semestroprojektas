package com.ktu.semestroprojektas.controllers;

import com.ktu.semestroprojektas.models.User;
import com.ktu.semestroprojektas.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Controller
public class RegistrationController {
    @Autowired
    UserService userService;

    @Autowired
    public RegistrationController(UserService userService) {
        this.userService = userService;
    }


    @RequestMapping(value= "/register", method = RequestMethod.GET)
    public String registrationForm() {
        return "register";
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String registrationValidation(HttpServletRequest request, Model model) {
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        String repeatedPassword = request.getParameter("repeated_password");
        User user = userService.findUserByEmail(email);

        List<String> errors = new ArrayList<>();
        Boolean success = false;

        if(!email.matches("[a-zA-Z0-9\\_\\+\\&\\*\\-]+(?:\\.[a-zA-Z0-9\\_\\+\\&\\*\\-]+)*\\@(?:[a-zA-Z0-9\\-]+)\\.[a-zA-Z]+"))
            errors.add("Invalid email");
        if(!password.equals(repeatedPassword))
            errors.add("Passwords do not match");
        if(user != null)
            errors.add("This email already exists");
        if(password.length() < 8)
            errors.add("Too short password");

        if(errors.isEmpty()) {
            success = true;
            userService.save(new User(email, userService.encode(password), "ROLE_USER"));
        }

        model.addAttribute("errors", errors);
        model.addAttribute("success", success);
        return "register";
    }

}
