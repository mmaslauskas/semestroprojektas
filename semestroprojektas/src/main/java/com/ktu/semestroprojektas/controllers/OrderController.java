package com.ktu.semestroprojektas.controllers;

import com.ktu.semestroprojektas.models.Comment;
import com.ktu.semestroprojektas.models.Order;
import com.ktu.semestroprojektas.models.RejectedOrder;
import com.ktu.semestroprojektas.models.User;
import com.ktu.semestroprojektas.repositories.OrderRepository;
import com.ktu.semestroprojektas.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.sound.midi.SysexMessage;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;

@Controller
public class OrderController {
    @Autowired
    OrderService orderService;
    @Autowired
    OrderRepository orderRepository;
    @Autowired
    CommentService commentService;
    @Autowired
    UserService userService;
    @Autowired
    EmailService emailService;
    @Autowired
    RejectedOrderService rejectedOrderService;


    @RequestMapping(value = "/leaveFeedback", method = RequestMethod.GET)
    public String leaveFeedback(HttpServletRequest request, Model model) {
        String orderId = request.getParameter("id");
        if(orderId.length() > 0) {
            Optional<Order> order = orderService.getById(Long.parseLong(orderId));
            if(order.isPresent()) {
                List<Comment> comments = commentService.findAllByOrderId(order.get().getId());
                model.addAttribute("order", order.get());
                model.addAttribute("comments", comments);
                model.addAttribute("isCourier", order.get().getCourier() != null);
                model.addAttribute("allowChange", userService.hasAdminAuthorities());
            }
        }
        return "Order/leaveFeedback";
    }

    @PostMapping(value = "/leaveFeedback")
    public String leaveFeedback(HttpServletRequest request) {
        String message = request.getParameter("feedback");

        if(message.length() < 1)
            return "redirect:/ordersList";

        if(request.getParameter("order_id") != null)  {
            Long id = Long.parseLong(request.getParameter("order_id"));
            Optional<Order> order = orderService.getById(id);

            if(order.isPresent()) {
                User user = userService.findUserByEmail(userService.getEmailFromSession());
                Comment comment = new Comment(message, order.get(), user);
                commentService.save(comment);
            }
        }
        return "redirect:/ordersList";
    }

    @GetMapping("/ordersList")
    public String getOrders(@PageableDefault(size = 10) Pageable pageable,
                               Model model, HttpServletRequest request) {

        if(userService.hasDispatcherAuthorties())
        {
            Page<Order> page;
            User user = userService.findUserByEmail(userService.getEmailFromSession());
            Map<String, String[]> params = request.getParameterMap();
            if(params.containsKey("title") && params.containsKey("category") && params.containsKey("dateFrom") &&
                    params.containsKey("dateTo") && params.containsKey("priceFrom") && params.containsKey("priceTo") &&
                params.containsKey("status"))
            {
                page = orderRepository.getFilteredDispatcherOrders(request.getParameter("status"),
                        request.getParameter("title"), request.getParameter("category"),
                        request.getParameter("priceFrom"), request.getParameter("priceTo"),
                        request.getParameter("dateFrom"), request.getParameter("dateTo"),
                        user.getId(),pageable);
            }
            else {
                page = orderRepository.getDispatcherOrders(user.getId(), pageable);
            }
            List<String> titles = orderRepository.getDistinctTitles();
            List<String> categories = orderRepository.getDistinctCategories();
            List<String> status = orderRepository.getDistinctStatus();
            model.addAttribute("page", page);
            model.addAttribute("titles", titles);
            model.addAttribute("categories", categories);
            model.addAttribute("status", status);
            return "orderListDispatcher";
        }

        else if(userService.hasCourierAuthorties()) {
            User user = userService.findUserByEmail(userService.getEmailFromSession());
            Page<Order> page = orderRepository.getCourierOrders(user.getId(), pageable);
            model.addAttribute("page", page);
            return "orderListCourier";
        }

        else if(userService.hasAdminAuthorities()) {
            Page<Order> page;
            User user = userService.findUserByEmail(userService.getEmailFromSession());
            Map<String, String[]> params = request.getParameterMap();
            if(params.containsKey("title") && params.containsKey("category") && params.containsKey("dateFrom") &&
                    params.containsKey("dateTo") && params.containsKey("priceFrom") && params.containsKey("priceTo") &&
                    params.containsKey("status"))
            {
                page = orderRepository.getFilteredOrders(request.getParameter("status"),
                        request.getParameter("title"), request.getParameter("category"),
                        request.getParameter("priceFrom"), request.getParameter("priceTo"),
                        request.getParameter("dateFrom"), request.getParameter("dateTo"),
                        pageable);
            }
            else {
                page = orderRepository.findAll(pageable);
            }
            List<String> titles = orderRepository.getDistinctTitles();
            List<String> categories = orderRepository.getDistinctCategories();
            List<String> status = orderRepository.getDistinctStatus();
            model.addAttribute("page", page);
            model.addAttribute("titles", titles);
            model.addAttribute("categories", categories);
            model.addAttribute("status", status);
            return "orderListDispatcher";
        }

        Page<Order> page = orderRepository.getDispatcherOrders(null, pageable);
        model.addAttribute("page", page);
        model.addAttribute("isUser", userService.hasUserAuthorties());
        return "Order/ordersList";
    }

    @PostMapping(value = "/updateStatus")
    public String updateStatus(@ModelAttribute("status") int statusNo, HttpServletRequest request) {

        String orderId = request.getParameter("order_id");
        String newStatus = "NEW ORDER STATUS";

        Long id = Long.parseLong(orderId);
        Optional<Order> order = orderService.getById(id);

        if(order.isPresent()) {
            User user = userService.findUserByEmail(userService.getEmailFromSession());
            Order updatedOrder = order.get();

            if (updatedOrder.getStatus().equalsIgnoreCase("Available"))
            {
                if (statusNo == 1) newStatus = "Accepted";
                else newStatus = "Rejected";
            }

            else if (updatedOrder.getStatus().equalsIgnoreCase("In transit"))
            {
                if (statusNo == 1) newStatus = "Delivered";
                else newStatus = "Failed to deliver";
            }

            else if (updatedOrder.getStatus().equalsIgnoreCase("Accepted"))
            {
                newStatus = "In transit";
            }

            if(newStatus.equalsIgnoreCase("Rejected"))
            {
                updatedOrder.setCourier(user);
                emailService.informStatusUpdate(updatedOrder, newStatus);
                updatedOrder.setCourier(null);

                RejectedOrder rejectedOrder = new RejectedOrder(updatedOrder, user);
                rejectedOrderService.save(rejectedOrder);
            }

            else {
                updatedOrder.setCourier(user);
                emailService.informStatusUpdate(updatedOrder, newStatus);
                updatedOrder.setStatus(newStatus);
                orderRepository.save(updatedOrder);
            }

        }

        return "redirect:/ordersList";
    }

    @RequestMapping(value = "/changeOrderDetails", method = RequestMethod.GET)
    public String changeOrderDetails(HttpServletRequest request, Model model) {
        String orderId = request.getParameter("id");
        if(orderId != null) {
            if (orderId.length() > 0) {
                Optional<Order> order = orderService.getById(Long.parseLong(orderId));
                if (order.isPresent()) {
                    List<User> dispatchers = userService.getDispatchers();
                    List<User> couriers = userService.getCouriers();
                    model.addAttribute("dispatchers", dispatchers);
                    model.addAttribute("couriers", couriers);
                    model.addAttribute("order", order.get());
                }
            }
        }

        return "Order/changeDetails";
    }

    @PostMapping(value = "/changeOrderDetails")
    public String changeOrderDetails(HttpServletRequest request) {
        String dispatcherId = request.getParameter("dispatcher");
        String courierId = request.getParameter("courier");
        String status = request.getParameter("status");
        String orderId = request.getParameter("order_id");

        if(orderId != null) {
            Optional<User> dispatcher = userService.getById(Long.parseLong(dispatcherId));
            Optional<User> courier = userService.getById(Long.parseLong(courierId));

            String status_ = "Available";
            switch(status) {
                case "1": status_ = "Available"; break;
                case "2": status_ = "Accepted"; break;
                case "3": status_ = "In transit"; break;
                case "4": status_ = "Delivered"; break;
                case "5": status_ = "Failed to deliver"; break;
                case "6": status_ = "Rejected"; break;
            }

            if(dispatcher.isPresent() && courier.isPresent()) {
                if("Available".equals(status_)) {
                    orderService.updateOrder(Long.parseLong(orderId),
                            status_,
                            null,
                            dispatcher.get());
                }
                else {
                    orderService.updateOrder(Long.parseLong(orderId),
                            status_,
                            courier.get(),
                            dispatcher.get());
                }
            }
        }

        return "redirect:/ordersList";
    }
}
