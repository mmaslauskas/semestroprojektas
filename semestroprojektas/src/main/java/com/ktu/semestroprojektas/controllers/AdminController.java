package com.ktu.semestroprojektas.controllers;

import com.ktu.semestroprojektas.models.Comment;
import com.ktu.semestroprojektas.models.Order;
import com.ktu.semestroprojektas.models.User;
import com.ktu.semestroprojektas.repositories.UserRepository;
import com.ktu.semestroprojektas.security.MyUserDetails;
import com.ktu.semestroprojektas.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
public class AdminController {
    @Autowired
    UserRepository userRepository;
    @Autowired
    UserService userService;

    @GetMapping("/admin/menu")
    public String adminMenu(@PageableDefault(size = 10) Pageable pageable,
                            Model model) {
        Page<User> page = userRepository.findAll(pageable);
        model.addAttribute("page", page);
        return "Admin/adminMenu";
    }

    @RequestMapping(value= "/admin/addUser", method = RequestMethod.GET)
    public String addUser() {
        return "Admin/addUser";
    }

    @RequestMapping(value = "/admin/addUser", method = RequestMethod.POST)
    public String addUserValidation(HttpServletRequest request, Model model) {
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        String repeatedPassword = request.getParameter("repeated_password");
        int roleNo = Integer.parseInt(request.getParameter("role"));
        User user = userService.findUserByEmail(email);

        List<String> errors = new ArrayList<>();
        Boolean success = false;

        if(email.length() < 4 || email.length() > 30)
            errors.add("Email must contain from 4 to 30 symbols");
        if(!password.equals(repeatedPassword))
            errors.add("Passwords do not match");
        if(user != null)
            errors.add("This email already exists");
        if(password.length() < 8)
            errors.add("Too short password");

        if(errors.isEmpty()) {
            String role = "";

            if(roleNo == 0)
                role = "ROLE_USER";
            if(roleNo == 1)
                role = "ROLE_DISPATCHER";
            if(roleNo == 2)
                role ="ROLE_COURIER";
            if(roleNo == 3)
                role ="ROLE_ADMIN";

            success = true;
            userService.save(new User(email, userService.encode(password), role));
        }

        model.addAttribute("errors", errors);
        model.addAttribute("success", success);
        return "Admin/addUser";
    }

    @RequestMapping(value = "/admin/editUser", method = RequestMethod.GET)
    public String editUser(HttpServletRequest request, Model model) {
        String userId = request.getParameter("id");

        if(userId.length() > 0) {
            Long id = Long.parseLong(userId);
            Optional<User> user = userService.getById(id);

            if(user.isPresent()) {
                model.addAttribute("user", user.get());
                return "Admin/editUser";
            }
        }
        return "redirect:/admin/menu";
    }

    @RequestMapping(value = "/admin/editUser", method = RequestMethod.POST)
    public String editUserValidation(HttpServletRequest request, Model model) {
        Long id = Long.parseLong(request.getParameter("user_id"));
        Optional<User> user = userService.getById(id);

        if(user.isPresent()) {
            User realUser = user.get();
            String oldEmail = realUser.getEmail();
            String email = request.getParameter("email");
            String password = request.getParameter("password");
            String repeatedPassword = request.getParameter("repeated_password");
            User userByEmail = userService.findUserByEmail(email);
            int roleNo = Integer.parseInt(request.getParameter("role"));

            List<String> errors = new ArrayList<>();
            Boolean success = false;

            if(email.length() < 4 || email.length() > 30)
                errors.add("Email must contain from 4 to 30 symbols");
            if(!password.equals(repeatedPassword))
                errors.add("Passwords do not match");
            if(userByEmail != null && !realUser.getEmail().equals(email))
                errors.add("This email already exists");
            if(password.length() < 8)
                errors.add("Too short password");

            if(errors.isEmpty()) {
                String role = "";

                if(roleNo == 0)
                    role = "ROLE_USER";
                if(roleNo == 1)
                    role = "ROLE_DISPATCHER";
                if(roleNo == 2)
                    role ="ROLE_COURIER";
                if(roleNo == 3)
                    role ="ROLE_ADMIN";

                success = true;
                realUser.setEmail(email);
                realUser.setPassword(userService.encode(password));
                realUser.setRole(role);

                userService.save(realUser);

                Object principal = userService.userLoggedInPrincipal(oldEmail);

                if(principal != null) {
                    userService.deleteUserSessions(principal);
                    if(userService.getEmailFromSession() == oldEmail)
                        return "redirect:/login";
                }
            }

            model.addAttribute("errors", errors);
            model.addAttribute("success", success);
            model.addAttribute("user", realUser);

            return "Admin/editUser";
        }

        return "redirect:/admin/menu";
    }

    @RequestMapping(value = "/admin/deleteUser", method = RequestMethod.GET)
    public String deleteUser(HttpServletRequest request, Model model) {
        String userId = request.getParameter("id");

        if(userId.length() > 0) {
            Long id = Long.parseLong(userId);
            Optional<User> user = userService.getById(id);

            if(user.isPresent()) {
                String email = user.get().getEmail();

                userService.deleteById(id);

                Object principal = userService.userLoggedInPrincipal(email);

                if(principal != null) {
                    userService.deleteUserSessions(principal);
                    if(userService.getEmailFromSession() == email)
                        return "redirect:/login";
                }
            }
        }
        return "redirect:/admin/menu";
    }

}
