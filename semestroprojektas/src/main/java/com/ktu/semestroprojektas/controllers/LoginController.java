package com.ktu.semestroprojektas.controllers;

import com.ktu.semestroprojektas.models.User;
import com.ktu.semestroprojektas.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import com.ktu.semestroprojektas.services.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class LoginController {
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String LoginForm() {
        return "login";
    }
}