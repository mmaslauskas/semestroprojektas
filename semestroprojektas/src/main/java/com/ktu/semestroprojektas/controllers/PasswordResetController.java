package com.ktu.semestroprojektas.controllers;

import com.ktu.semestroprojektas.models.PasswordResetToken;
import com.ktu.semestroprojektas.models.User;
import com.ktu.semestroprojektas.services.EmailService;
import com.ktu.semestroprojektas.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import com.ktu.semestroprojektas.services.PasswordResetService;
import org.springframework.web.bind.support.SessionStatus;

import javax.servlet.http.HttpServletRequest;
import java.util.Calendar;
import java.util.UUID;

@Controller
public class PasswordResetController {

     @Autowired
     private PasswordResetService resetService;
     @Autowired
     private EmailService emailService;
     @Autowired
     private UserService userService;


    @RequestMapping(value = "/forgot", method = RequestMethod.POST)
    public String sendPasswordResetToken(HttpServletRequest request, Model model)
    {
        String email = request.getParameter("email");

        if (!email.matches("[a-zA-Z0-9\\_\\+\\&\\*\\-]+(?:\\.[a-zA-Z0-9\\_\\+\\&\\*\\-]+)*\\@(?:[a-zA-Z0-9\\-]+)\\.[a-zA-Z]+")) {
            model.addAttribute("message", "Invalid email");
            return "forgotpassword";
        }

        User user = userService.findUserByEmail(email);

        if (user == null)
        {
            model.addAttribute("message", "There is no user with this email address. Try again.");
            return "forgotpassword";
        }

        String token = UUID.randomUUID().toString();
        PasswordResetToken resetToken = new PasswordResetToken(token, user);
        String appUrl = request.getScheme() + "://" + request.getServerName();

        emailService.sendResetLink(resetToken, appUrl);
        resetService.save(resetToken);

        model.addAttribute("message", "Password reset link has been successfully sent to your email.");
        return "forgotpassword";
    }

    @RequestMapping(value = "/reset", method = RequestMethod.GET)
    public String displayUpdatePasswordPage(Model model, @RequestParam String token)
    {
        PasswordResetToken resetToken = resetService.findToken(token);

        if (resetToken != null)
        {
            Calendar calendar = Calendar.getInstance();

            if (resetToken.getExpiryDate().getTime().after(calendar.getTime()))
            {
                model.addAttribute("resetToken", token);
                return "resetpassword";
            }

            else
            {
                model.addAttribute("message", "Password reset link has expired.\n Ask for a new one.");
                model.addAttribute("path", "/forgot");
                return "alert";
            }
        }

        else
        {
            model.addAttribute("message", "Invalid password reset link.\n Ask for a new one.");
            model.addAttribute("path", "/forgot");
            return "alert";
        }
    }

    @RequestMapping(value = "/reset", method = RequestMethod.POST)
    public String updatePassword(Model model,
                                 @ModelAttribute("token") String token,
                                 HttpServletRequest request)
    {
        PasswordResetToken resetToken = resetService.findToken(token);

        if (resetToken == null)
        {
            model.addAttribute("message", "Password reset link has been used.\n Ask for a new one.");
            model.addAttribute("path", "/forgot");
            return "alert";
        }

        User user = resetToken.getUser();

        String password = request.getParameter("password");
        String repeatedPassword = request.getParameter("repeated_password");

        if (password.length() < 8 || repeatedPassword.length() < 8)
        {
            model.addAttribute("resetToken", token);
            model.addAttribute("message",
                    "Password should have at least 8 symbols.\n Enter both passwords in order to continue.");
            return "resetpassword";
        }

        if (!password.equals(repeatedPassword))
        {
            model.addAttribute("resetToken", token);
            model.addAttribute("message", "Passwords don't match. Try again.");
            return "resetpassword";
        }

        user.setPassword(userService.encode(password));
        userService.save(user);
        resetService.deleteToken(token);

        model.addAttribute("message", "Password successfully updated.\n You can now login.");
        model.addAttribute("path", "/login");
        return "alert";
    }

    @RequestMapping(value= "/forgot", method = RequestMethod.GET)
    public String passwordResetForm() {
        return "forgotpassword";
    }

}
