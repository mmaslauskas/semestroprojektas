package com.ktu.semestroprojektas.controllers;

import com.ktu.semestroprojektas.models.User;
import com.ktu.semestroprojektas.security.MyUserDetails;
import com.ktu.semestroprojektas.security.MyUserDetails;
import com.ktu.semestroprojektas.services.MyUserDetailsService;
import com.ktu.semestroprojektas.services.UserService;
import org.apache.coyote.Request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

@Controller
public class ChangePasswordController {


    private final UserService service;
    private BCryptPasswordEncoder encoder;

    @Autowired
    public ChangePasswordController(UserService service) {
        this.service = service;
    }

    @RequestMapping(value = "/changepassword", method = RequestMethod.GET)
    public String changePasswordform(HttpServletRequest request ,Model model) {
        model.addAttribute("email", request.getParameter("email"));
        return "changepassword";
    }
    @RequestMapping(value = "/changepassword", method = RequestMethod.POST)
    public String changePasswordValidation(HttpServletRequest request, Model model) {
        String oldpassword = request.getParameter("oldpassword");
        String password = request.getParameter("newpassword");
        String repeatedPassword = request.getParameter("newpasswordagain");
        String email = request.getParameter("email");
        User user;

        if(email.length() == 0 || oldpassword.length() == 0 || password.length() == 0 || repeatedPassword.length() == 0) {
            model.addAttribute("notAllFilled", true);
            return "changepassword";
        }

        if(service.findUserByEmail(email) == null) {
            model.addAttribute("wrongEmail", true);
            return "changepassword";
        }
        user = service.findUserByEmail(email);

        if(!password.equals(repeatedPassword)) {
            model.addAttribute("passwordDontMatch", true);
            return "changepassword";
        }
        if(!service.matches(oldpassword, user.getPassword())) {
            model.addAttribute("incorrectPassword", true);
            return "changepassword";
        }
        if(password.length() < 8) {
            model.addAttribute("passwordBoundariesError", true);
            return "changepassword";
        }

        user.setPassword(service.encode(password));
        service.save(user);
        model.addAttribute("success", true);
        return "changepassword";
    }
}
