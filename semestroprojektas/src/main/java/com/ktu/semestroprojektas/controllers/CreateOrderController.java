package com.ktu.semestroprojektas.controllers;

import com.ktu.semestroprojektas.models.Order;
import com.ktu.semestroprojektas.models.User;
import com.ktu.semestroprojektas.services.OrderService;
import com.ktu.semestroprojektas.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Calendar;
import java.text.SimpleDateFormat;
import java.util.List;

@Controller
public class CreateOrderController {
    @Autowired
    OrderService orderService;

    @Autowired
    UserService userService;

    @Autowired
    public CreateOrderController(OrderService service,
                                 UserService userS) {
        this.orderService = service;
        this.userService = userS;
    }

    @RequestMapping(value = "/createOrder", method = RequestMethod.GET)
    public String createOrderGet() {
        return "createOrder";
    }

    @RequestMapping(value = "/createOrder", method = RequestMethod.POST)
    public String createOrderPost(HttpServletRequest request, Model model) {

        List<String> errors = new ArrayList<>();
        Boolean success = false;

        String title = request.getParameter("title");
        String category = request.getParameter("category");
        String price = request.getParameter("price");
        String weight = request.getParameter("weight");
        String dimensions = request.getParameter("dimensions");
        String origin = request.getParameter("origin");
        String destination = request.getParameter("destination");
        String rec_name = request.getParameter("recipient_name");
        String rec_number = request.getParameter("recipient_number");
        String rec_email = request.getParameter("recipient_email");

        String[] arr = {title, category, price, weight, dimensions, origin, destination, rec_email, rec_name, rec_number};
        Boolean isEmpty = false;
        for(String i : arr) {
            if(i.length() == 0) {
                errors.add("No fields should be empty");
                isEmpty = true;
                break;
            }
        }

        if(!isEmpty) {

            double price_ = Double.parseDouble(price);
            double weight_ = Double.parseDouble(weight);

            User user = userService.findUserByEmail(userService.getEmailFromSession());

            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            Date currentDate = new Date(System.currentTimeMillis());
            String date = format.format(currentDate);

            // string parsinimas i Date:
            /*Date parsed;
            try {
                parsed = format.parse(date);
            } catch (ParseException ex) {
                parsed = null;
            }*/


            if (price_ < 0) {
                errors.add("Price cannot be negative");
            }
            if (weight_ < 0) {
                errors.add("Weight cannot be negative");
            }
            if (!rec_number.matches("\\+*[0-9]+-*[0-9]+")) {
                errors.add("Invalid phone number");
            }
            if (!rec_email.matches("[a-zA-Z0-9\\_\\+\\&\\*\\-]+(?:\\.[a-zA-Z0-9\\_\\+\\&\\*\\-]+)*\\@(?:[a-zA-Z0-9\\-]+)\\.[a-zA-Z]+")) {
                errors.add("Invalid email");
            }
            if (!dimensions.matches("[0-9]+x[0-9]+x[0-9]+")) {
                errors.add("Dimensions should be in a form (without quotes): \"number\"x\"number\"x\"number\"");
            }

            if (dimensions.matches("[0-9]+x[0-9]+x[0-9]+")) {
                String[] values = dimensions.split("x");
                for (String i : values) {
                    int val = Integer.parseInt(i);
                    if (val < 0) {
                        errors.add("Dimensions cannot be negative");
                        break;
                    }
                }
            }


            if (errors.isEmpty()) {
                if (user != null && (userService.hasDispatcherAuthorties() || userService.hasAdminAuthorities())) {
                    success = true;

                    orderService.save(new Order(title, category, price_, weight_, dimensions, origin, destination,
                            user, "Available", rec_name, rec_number, rec_email, date));
                }

            }

        }
        model.addAttribute("errors", errors);
        model.addAttribute("success", success);

        return "createOrder";
    }

}
