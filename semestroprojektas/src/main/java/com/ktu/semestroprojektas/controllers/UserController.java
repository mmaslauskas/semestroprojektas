package com.ktu.semestroprojektas.controllers;

import com.ktu.semestroprojektas.security.MyUserDetails;
import com.ktu.semestroprojektas.services.OrderService;
import com.ktu.semestroprojektas.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.session.SessionInformation;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.Collection;
import java.util.List;

@Controller
public class UserController {
    @Autowired
    UserService userService;
    @Autowired
    OrderService orderService;
    @Autowired
    private SessionRegistry sessionRegistry;

    @GetMapping("/")
    public String loggedIn(Model model) {
        if (userService.hasUserAuthorties() || userService.hasCourierAuthorties() ||
                userService.hasDispatcherAuthorties() || userService.hasAdminAuthorities())
        {
            model.addAttribute("hasAdminAuthorities", userService.hasAdminAuthorities());
            model.addAttribute("email", userService.getEmailFromSession());
            return "home";
        }
        return "login";
    }

    @GetMapping("/chooseCourierOrDispatcher")
    public String chooseCourierOrDispatcher(Model model) {
        List<Object> principals = sessionRegistry.getAllPrincipals();

        for (Object principal: principals) {
            UserDetails details = (UserDetails) principal;
        }


        return "chooseRole";
    }

    @PostMapping("chooseRole")
    public String choosegRole(@ModelAttribute("role") int roleNo) {
        String role = "USER";

        if(roleNo == 1)
            role = "ROLE_DISPATCHER";
        if(roleNo == 2)
            role ="ROLE_COURIER";

        userService.updateUserRole(role, userService.getEmailFromSession());

        Object principal = userService.userLoggedInPrincipal(userService.getEmailFromSession());
        userService.deleteUserSessions(principal);
        return "redirect:/";
    }
}
