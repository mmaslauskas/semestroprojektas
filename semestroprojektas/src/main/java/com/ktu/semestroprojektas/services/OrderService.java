package com.ktu.semestroprojektas.services;

import com.ktu.semestroprojektas.models.Order;
import com.ktu.semestroprojektas.models.User;
import com.ktu.semestroprojektas.repositories.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class OrderService {
    private OrderRepository orderRepository;

    @Autowired
    public OrderService(OrderRepository repository) {
        this.orderRepository = repository;
    }

    public Page<Order> getAll(int page, int size) {
        return orderRepository.findAll(PageRequest.of(page, size));
    }

    public Optional<Order> getById(Long id){
        return orderRepository.findById(id);
    }
    public Long getCount() {
        return orderRepository.count();
    }

    public void save(Order order) {
        orderRepository.save(order);
    }

    public void updateOrder(Long id, String status, User courier, User dispatcher) {
        orderRepository.updateOrder(id, dispatcher, courier, status);
    }
}
