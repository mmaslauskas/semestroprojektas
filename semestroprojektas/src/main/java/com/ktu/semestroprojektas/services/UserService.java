package com.ktu.semestroprojektas.services;

import com.ktu.semestroprojektas.models.User;
import com.ktu.semestroprojektas.repositories.UserRepository;
import com.ktu.semestroprojektas.security.MyUserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.session.SessionInformation;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    private UserRepository repository;
    private BCryptPasswordEncoder encoder;

    @Autowired
    private SessionRegistry sessionRegistry;

    @Autowired
    public UserService(UserRepository repository, BCryptPasswordEncoder encoder) {
        this.encoder = encoder;
        this.repository = repository;
    }

    public void deleteById(Long id) {
        repository.deleteById(id);
    }

    public Optional<User> getById(Long id) {
        return repository.findById(id);
    }

    public List<User> getAll() {
        return repository.findAll();
    }

    public User findUserByEmail(String email)
    {
        return repository.findByEmail(email);
    }

    public String encode(String str) {
        return encoder.encode(str);
    }

    public boolean matches(String raw, String encoded) { return encoder.matches(raw, encoded);}

    public void save(User user)
    {
        repository.save(user);
    }

    public Boolean hasUserAuthorties() {
        return SecurityContextHolder.getContext().getAuthentication().getAuthorities().stream()
                .anyMatch(grantedAuthority -> grantedAuthority.getAuthority().equals("ROLE_USER"));
    }

    public Boolean hasDispatcherAuthorties() {
        return SecurityContextHolder.getContext().getAuthentication().getAuthorities().stream()
                .anyMatch(grantedAuthority -> grantedAuthority.getAuthority().equals("ROLE_DISPATCHER"));
    }

    public Boolean hasCourierAuthorties() {
        return SecurityContextHolder.getContext().getAuthentication().getAuthorities().stream()
                .anyMatch(grantedAuthority -> grantedAuthority.getAuthority().equals("ROLE_COURIER"));
    }

    public Boolean hasAdminAuthorities() {
        return SecurityContextHolder.getContext().getAuthentication().getAuthorities().stream()
                .anyMatch(grantedAuthority -> grantedAuthority.getAuthority().equals("ROLE_ADMIN"));
    }

    public String getEmailFromSession() {
        return SecurityContextHolder.getContext().getAuthentication().getName();
    }

    public void updateUserRole(String role, String email) {
        repository.updateRole(role, email);
    }

    public List<User> getDispatchers() {
        return repository.findByRole("ROLE_DISPATCHER");
    }

    public List<User> getCouriers() {
        return repository.findByRole("ROLE_COURIER");
    }

    public Object userLoggedInPrincipal(String email) {
        List<Object> principals = sessionRegistry.getAllPrincipals();

        for (Object principal: principals) {
            UserDetails details = (UserDetails) principal;
            if(details.getUsername().equals(email))
                return principal;
        }
        return null;
    }

    public void deleteUserSessions(Object principal) {
        List<SessionInformation> sessionInformations = sessionRegistry.getAllSessions(principal, true);

        for (SessionInformation sessionInformation : sessionInformations) {
            sessionInformation.expireNow();
        }
    }
}
