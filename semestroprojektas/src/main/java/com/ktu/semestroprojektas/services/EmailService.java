package com.ktu.semestroprojektas.services;

import com.ktu.semestroprojektas.models.Order;
import com.ktu.semestroprojektas.models.PasswordResetToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class EmailService {

    private JavaMailSender mailSender;
    @Value("${server.port}")
    private String serverPort;

    @Autowired
    public EmailService(JavaMailSender sender) {mailSender = sender;}

    @Async
    public void sendResetLink(PasswordResetToken resetToken, String appUrl)
    {
        SimpleMailMessage message = new SimpleMailMessage();

        message.setTo(resetToken.getUser().getEmail());
        message.setSubject("Password Reset Request");
        message.setText("To reset your password, click the link below:\n" + appUrl + ":" + serverPort
                + "/reset?token=" + resetToken.getToken());

        mailSender.send(message);
    }

    public void informStatusUpdate(Order order, String newStatus)
    {
        SimpleMailMessage message = new SimpleMailMessage();

        message.setTo(order.getDispatcher().getEmail());

        if(newStatus.equalsIgnoreCase("Rejected"))
        {
            message.setSubject("Rejection of Order ID:" + order.getId());
            message.setText("Courier ID:" + order.getCourier().getId() + " has rejected Order ID:" + order.getId());
        }

        else {
            message.setSubject("Order ID:" + order.getId() + " status update");
            message.setText("Courier ID:" + order.getCourier().getId() + " has changed Order ID:" + order.getId() + " status from " +
                    order.getStatus() + " to " + newStatus);
        }

        mailSender.send(message);
    }

}
