package com.ktu.semestroprojektas.services;

import com.ktu.semestroprojektas.models.Comment;
import com.ktu.semestroprojektas.repositories.CommentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentService {
    @Autowired
    CommentRepository commentRepository;

    public void save(Comment comment) {
        commentRepository.save(comment);
    }

    public List<Comment> findAllByOrderId(Long orderId) {
        return commentRepository.findAllByOrderId(orderId);
    }
}
