package com.ktu.semestroprojektas.services;

import com.ktu.semestroprojektas.models.PasswordResetToken;
import com.ktu.semestroprojektas.repositories.PassResetTokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PasswordResetService {

    private PassResetTokenRepository repository;

    @Autowired
    public PasswordResetService(PassResetTokenRepository repository) {this.repository = repository;}

    public PasswordResetToken findToken(String token)
    {
        return repository.findByToken(token);
    }

    public void save(PasswordResetToken token)
    {
        repository.save(token);
    }

    public void deleteToken(String token)
    {
        PasswordResetToken resetToken = repository.findByToken(token);
        repository.delete(resetToken);
    }

}
