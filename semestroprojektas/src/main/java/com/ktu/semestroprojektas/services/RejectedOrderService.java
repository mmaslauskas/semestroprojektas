package com.ktu.semestroprojektas.services;

import com.ktu.semestroprojektas.models.RejectedOrder;
import com.ktu.semestroprojektas.repositories.OrderRepository;
import com.ktu.semestroprojektas.repositories.RejectedOrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RejectedOrderService {
    private RejectedOrderRepository rejectedOrderRepository;

    @Autowired
    public RejectedOrderService(RejectedOrderRepository repository) {
        this.rejectedOrderRepository = repository;
    }

    public void save(RejectedOrder order) {rejectedOrderRepository.save(order);}
}
