package com.ktu.semestroprojektas.services;

import com.ktu.semestroprojektas.models.User;
import com.ktu.semestroprojektas.repositories.UserRepository;
import com.ktu.semestroprojektas.security.MyUserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


@Service
public class MyUserDetailsService implements UserDetailsService {
    @Autowired
    UserRepository userRepository;


    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        User user = userRepository.findByEmail(s);

        MyUserDetails details = new MyUserDetails(user);

        return details;
    }
}

