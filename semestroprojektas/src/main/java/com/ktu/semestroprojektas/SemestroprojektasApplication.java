package com.ktu.semestroprojektas;

import com.ktu.semestroprojektas.repositories.UserRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
@EnableJpaRepositories(basePackageClasses = UserRepository.class)
public class SemestroprojektasApplication {

	public static void main(String[] args) {
		SpringApplication.run(SemestroprojektasApplication.class, args);
	}
}
