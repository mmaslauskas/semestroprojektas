package com.ktu.semestroprojektas.controllers;

import com.ktu.semestroprojektas.models.User;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class AdminControllerTest {

    @Test
    public void testeditUserIdLength() {
        String idString = "151";
        assertTrue(idString.length() > 0);
    }

    @Test
    public void testParseLong() {
        String idString = "151";
        Long longVal = Long.parseLong(idString);
        assertTrue(longVal instanceof Long);
    }

    @Test
    public void testUserPresent() {
        Optional<User> user = Optional.of(new User("test@gmail.com", "test", "ROLE_USER"));
        assertTrue(user.isPresent());
    }


    @Test
    public void testUserRealUser() {
        Optional<User> user = Optional.of(new User("test@gmail.com", "test", "ROLE_USER"));
        assertTrue(user.get() instanceof User);
    }

    @Test
    public void testEmailFromUser() {
        Optional<User> user = Optional.of(new User("test@gmail.com", "test", "ROLE_USER"));
        User realUser = user.get();
        String email = "test@gmail.com";
        assertTrue(realUser.getEmail().equals(email));
    }

    @Test
    public void testParseInt() {
        String num = "143";
        int no = Integer.parseInt(num);
        assertTrue(no == (int) no);
    }

    @Test
    public void testEmailLength() {
        String email = "test@gmail.com";
        assertTrue(email.length() >= 4 && email.length() <= 30);
    }

    @Test
    public void testPasswordsNotEqual() {
        String pass1 = "pass1";
        String pass2 = "pass2";
        assertFalse(pass1.equals(pass2));
    }

    @Test
    public void testErrorsEmpty() {
        List<String> errors = new ArrayList<>();
        assertTrue(errors.isEmpty());
    }

    @Test
    public void testRoleNo() {
        int roleNo = 5;
        assertTrue(roleNo == 5);
    }

    @Test
    public void testRoleByRoleNo() {
        int roleNo = 5;
        String role = "";

        if(roleNo == 1)
            role = "test";
        if(roleNo == 5)
            role = "test2";
        assertTrue(role.equals("test2"));
    }

    @Test
    public void testSetUserEmail() {
        Optional<User> user = Optional.of(new User("test@gmail.com", "test", "ROLE_USER"));
        User realUser = user.get();
        String email = "test2@gmail.com";
        realUser.setEmail(email);
        assertTrue(realUser.getEmail().equals("test2@gmail.com"));
    }
}