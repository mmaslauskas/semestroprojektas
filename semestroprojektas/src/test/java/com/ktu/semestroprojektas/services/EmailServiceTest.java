package com.ktu.semestroprojektas.services;

import com.ktu.semestroprojektas.models.Order;
import com.ktu.semestroprojektas.models.PasswordResetToken;
import com.ktu.semestroprojektas.models.User;
import com.ktu.semestroprojektas.repositories.PassResetTokenRepository;
import javassist.Loader;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.when;

class EmailServiceTest {

    private JavaMailSender mailSender;
    @Value("${server.port}")
    private String serverPort;
    private SimpleMailMessage message;
    private PasswordResetToken resetToken;
    private Order order;
    private EmailService service;
    private User user;

    @BeforeEach
    void setUp()
    {
        resetToken = new PasswordResetToken("123456789", new User("vardenis.pavardenis@gmail.com",
                "123", "ROLE_USER"));
        user = new User("vardenis.pavardenis@gmail.com", "asas", "ROLE_DISPATCHER");
        mailSender = mock(JavaMailSender.class);
        service = new EmailService(mailSender);
        order = new Order("Žaislas", "Medis", 20.50, 4.99, "20x20x20", "Kinija", "Rusija", user, "Available", "Stasys",
                "86868686", "Kazas@hotmail.ru", "2018-04-45");
    }

    @Test
    public void testSendResetLink()
    {
        SimpleMailMessage message = new SimpleMailMessage();

        message.setTo(resetToken.getUser().getEmail());
        message.setSubject("Password Reset Request");
        message.setText("To reset your password, click the link below:\n" + "test" + ":" + serverPort
                + "/reset?token=" + resetToken.getToken());

        service.sendResetLink(resetToken, "test");
        verify(mailSender).send(message);

    }

    @Test
    public void testStatusUpdateReject()
    {

        order.setCourier(user);
        SimpleMailMessage message = new SimpleMailMessage();

        message.setTo(order.getDispatcher().getEmail());
        message.setSubject("Rejection of Order ID:" + order.getId());
        message.setText("Courier ID:" + order.getCourier().getId() + " has rejected Order ID:" + order.getId());

        service.informStatusUpdate(order, "Rejected");
        verify(mailSender).send(message);

    }

    @Test
    public void testStatusUpdateOther()
    {

        order.setCourier(user);
        SimpleMailMessage message = new SimpleMailMessage();

        message.setTo(order.getDispatcher().getEmail());
        message.setSubject("Order ID:" + order.getId() + " status update");
        message.setText("Courier ID:" + order.getCourier().getId() + " has changed Order ID:" + order.getId() + " status from " +
                order.getStatus() + " to " + "Accepted");

        service.informStatusUpdate(order, "Accepted");
        verify(mailSender).send(message);

    }
}