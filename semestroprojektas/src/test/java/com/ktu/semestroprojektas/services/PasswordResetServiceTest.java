package com.ktu.semestroprojektas.services;

import com.ktu.semestroprojektas.models.PasswordResetToken;
import com.ktu.semestroprojektas.models.User;
import com.ktu.semestroprojektas.repositories.PassResetTokenRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.mockito.Mockito.*;

class PasswordResetServiceTest {

    private PasswordResetToken resetToken;
    private PassResetTokenRepository repository;
    private PasswordResetService service;


    @BeforeEach
    void setUp()
    {
        resetToken = new PasswordResetToken("123456789", new User("vardenis.pavardenis@gmail.com",
                "123", "ROLE_USER"));

        repository = mock(PassResetTokenRepository.class);
        when(repository.findByToken(resetToken.getToken())).thenReturn(resetToken);
        when(repository.findByToken(any())).thenReturn(null);
        service = new PasswordResetService(repository);
    }

    @Test
    public void testFindToken()
    {
        service.findToken(resetToken.getToken());
        verify(repository).findByToken(resetToken.getToken());
    }

    @Test
    public void testSaveToken()
    {
        service.save(resetToken);
        verify(repository).save(resetToken);
    }

    @Test
    public void testDeleteTokenFind()
    {
        service.deleteToken(resetToken.getToken());
        verify(repository).findByToken(resetToken.getToken());
    }
}